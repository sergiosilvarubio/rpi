/*
 *  RPLIDAR
 *  Ultra Simple Data Grabber Demo App
 *
 *  Copyright (c) 2009 - 2014 RoboPeak Team
 *  http://www.robopeak.com
 *  Copyright (c) 2014 - 2019 Shanghai Slamtec Co., Ltd.
 *  http://www.slamtec.com
 *
 */
/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include "rplidar.h" //RPLIDAR standard sdk, all-in-one header

#ifndef _countof
#define _countof(_Array) (int)(sizeof(_Array) / sizeof(_Array[0]))
#endif

#include <unistd.h>
static inline void delay(_word_size_t ms){
    while (ms>=1000) {
        usleep(1000*1000);
        ms-=1000;
    };
    if (ms!=0)
        usleep(ms*1000);
}

using namespace rp::standalone::rplidar;

bool checkRPLIDARHealth(RPlidarDriver * drv)
{
    u_result     op_result;
    rplidar_response_device_health_t healthinfo;

    op_result = drv->getHealth(healthinfo);
    if (IS_OK(op_result)) { // the macro IS_OK is the preperred way to judge whether the operation is succeed.
        printf("RPLidar health status : %d\n", healthinfo.status);
        if (healthinfo.status == RPLIDAR_STATUS_ERROR) {
            fprintf(stderr, "Error, rplidar internal error detected. Please reboot the device to retry.\n");
            // enable the following code if you want rplidar to be reboot by software
            // drv->reset();
            return false;
        } else {
            return true;
        }
    } else {
        fprintf(stderr, "Error, cannot retrieve the lidar health code: %x\n", op_result);
        return false;
    }
}

#include <signal.h>
bool ctrl_c_pressed;
void ctrlc(int)
{
    ctrl_c_pressed = true;
}

#include <iostream>
#include <iomanip>
//#include <cmath.h>
//#include <Eigen/Dense>

int main(int argc, const char * argv[])
{
    const char * opt_com_path = NULL;
    _u32         baudrateArray[2] = {115200, 256000};
    _u32         opt_com_baudrate = 0;
    u_result     op_result;

    bool useArgcBaudrate = false;

    //printf("Ultra simple LIDAR data grabber for RPLIDAR.\n Version: "RPLIDAR_SDK_VERSION"\n");
    std::cout << "Ultra simple LIDAR data grabber for RPLIDAR.\n Version: " << RPLIDAR_SDK_VERSION << std::endl;

    // read serial port from the command line...
    if (argc > 1) opt_com_path = argv[1]; // or set to a fixed value: e.g. "/dev/ttyUSB0"

    // read baud rate from the command line if specified...
    if (argc > 2) {
        opt_com_baudrate = strtoul(argv[2], NULL, 10);
        useArgcBaudrate = true;
    }

    if (!opt_com_path) {
        // use default com port
        opt_com_path = "/dev/ttyUSB0";
    }

    // create the driver instance
	RPlidarDriver * drv = RPlidarDriver::CreateDriver(DRIVER_TYPE_SERIALPORT);
    if (!drv) {
        fprintf(stderr, "insufficent memory, exit\n");
        exit(-2);
    }
    
    rplidar_response_device_info_t devinfo;
    bool connectSuccess = false;
    // make connection...
    if(useArgcBaudrate)
    {
        if(!drv)
            drv = RPlidarDriver::CreateDriver(DRIVER_TYPE_SERIALPORT);
        if (IS_OK(drv->connect(opt_com_path, opt_com_baudrate)))
        {
            op_result = drv->getDeviceInfo(devinfo);

            if (IS_OK(op_result)) 
            {
                connectSuccess = true;
            }
            else
            {
                delete drv;
                drv = NULL;
            }
        }
    }
    else
    {
        size_t baudRateArraySize = (sizeof(baudrateArray))/ (sizeof(baudrateArray[0]));
        for(size_t i = 0; i < baudRateArraySize; ++i)
        {
            if(!drv)
                drv = RPlidarDriver::CreateDriver(DRIVER_TYPE_SERIALPORT);
            if(IS_OK(drv->connect(opt_com_path, baudrateArray[i])))
            {
                op_result = drv->getDeviceInfo(devinfo);

                if (IS_OK(op_result)) 
                {
                    connectSuccess = true;
                    break;
                }
                else
                {
                    delete drv;
                    drv = NULL;
                }
            }
        }
    }

    if (!connectSuccess) {
        fprintf(stderr, "Error, cannot bind to the specified serial port %s.\n", opt_com_path);
        goto on_finished;
    }

    // print out the device serial number, firmware and hardware version number..
    printf("RPLIDAR S/N: ");
    for (int pos = 0; pos < 16; ++pos) {
        printf("%02X", devinfo.serialnum[pos]);
    }

    printf("\n"
            "Firmware Ver: %d.%02d\n"
            "Hardware Rev: %d\n"
            , devinfo.firmware_version>>8
            , devinfo.firmware_version & 0xFF
            , (int)devinfo.hardware_version);

    // check health...
    if (!checkRPLIDARHealth(drv)) {
        goto on_finished;
    }

    signal(SIGINT, ctrlc);
    
    drv->startMotor();
    // start scan...
    drv->startScan(0, 1);

    // fetech result and print it out...
    while (1)
    {
        rplidar_response_measurement_node_hq_t nodes[8192];
        size_t count = _countof(nodes);

        op_result = drv->grabScanDataHq(nodes, count);

        if (IS_OK(op_result)) {
            drv->ascendScanData(nodes, count);

            //Eigen Vector3d x(1,2,3);

            /*
            float angle90 = 0.0;
            float angle180 = 0.0;
            float angle270 = 0.0;
            float angle360 = 0.0;

            float distance90 = 0.0;
            float distance270 = 0.0;

            float distance0 = 0.0;
            float distance180 = 0.0;
            */

            for (int pos = 0; pos < (int)count ; ++pos) {
                //double angle = (nodes[pos].angle_z_q14 * 90.f / (1 << 14)); // degrees
                //double distance = (nodes[pos].dist_mm_q2/4.0f);
                //int quality (nodes[pos].quality);

                /*
                float angle_r = angle * (M_PI/180.0);
                float x = distance * cos((M_PI/2.0)-angle_r);
                float y = distance * sin((M_PI/2.0)-angle_r);

                if ( angle <= 60 ) {
                    float angle_t = trunc(angle);
                    //std::cout << std::fixed << std::setprecision(2) << angle_t << ", " << std::fixed << std::setprecision(2) << distance << std::endl;
                    std::cout << std::fixed << std::setprecision(2) << angle_t << ","
                              << std::fixed << std::setprecision(2) << distance << ","
                              << std::fixed << std::setprecision(2) << x << ","
                              << std::fixed << std::setprecision(2) << y << ","
                              << std::endl;
                }
                */

                /*
                if ( angle <= 60 || 300 <= angle) {
                    std::cout << "Angle: " << std::fixed << std::setprecision(2) << angle << std::endl;
                    std::cout << "Distance: " << std::fixed << std::setprecision(2) << distance << std::endl;
                    std::cout << "Quality: " << quality << std::endl;
                }
                */

                /*
                printf("%s theta: %03.2f Dist: %08.2f Q: %d \n", 
                    (nodes[pos].flag & RPLIDAR_RESP_MEASUREMENT_SYNCBIT) ?"S ":"  ", 
                    (nodes[pos].angle_z_q14 * 90.f / (1 << 14)), 
                    nodes[pos].dist_mm_q2/4.0f,
                    nodes[pos].quality);
                */

                /*
                float angle_t = trunc(angle);
                if (angle_t == 90)
                    distance90 = distance;
                else if (angle_t == 270)
                    distance270 = distance;

                if (angle_t == 0)
                    distance0 = distance;
                else if (angle_t == 180)
                    distance180 = distance;

                std::cout << "Distance X: " << std::fixed << std::setprecision(2) << (distance90+distance270) << std::endl;
                std::cout << "Distance Y: " << std::fixed << std::setprecision(2) << (distance0+distance180) << std::endl;
                */

                /*
                std::cout << "Angle: " << std::fixed << std::setprecision(2) << angle << std::endl;
                std::cout << "Distance: " << std::fixed << std::setprecision(2) << distance << std::endl;
                std::cout << "Quality: " << quality << std::endl;
                */
            }
        }

        if (ctrl_c_pressed){ 
            break;
        }
    }

    drv->stop();
    drv->stopMotor();
    // done!
on_finished:
    RPlidarDriver::DisposeDriver(drv);
    drv = NULL;
    return 0;
}

