# Carpetas

## app

Es el primer proyecto que edité. Contiene el SDK [https://github.com/Slamtec/rplidar_sdk](https://github.com/Slamtec/rplidar_sdk) y está escrito en C++. Para compilar utiliza [make](https://www.gnu.org/software/make/manual/html_node/Introduction.html)

```bash
cd app
make
```

Las salidas se guardan en la carpeta:
```
app/output/Linux/Release
```

## catkin_ws

> Nota: Antes de utilizar se debe instalar ROS, ver pasos más abajo.

Es el proyecto para trabajar con (ROS)[http://wiki.ros.org]. El mejor lugar para estudiar sobre ROS es la página oficial [http://wiki.ros.org/ROS/Tutorials](http://wiki.ros.org/ROS/Tutorials).

La base del proyecto está basada en el proyecto oficial de RPLidar en ROS: (https://github.com/Slamtec/rplidar_ros)[https://github.com/Slamtec/rplidar_ros]

Para ejecutar el proyecto que está en desarrollo:

```bash
# in terminal 0
cd catkin_ws
catkin_make
source devel/setup.bash

# in new terminal 1
roscore

# in terminal 0
roslaunch robot_ihabita test_rplidar.launch
```

## utils

Contiene código de prueba relacionado a la programación de la regresion lineal.

## docs

Contiene documentos oficiales

## exec

Contiene programas ejecutables que se pueden utilizar en Windows

## Instalación de ROS en Raspberry Pi 4 Model B

1. Descargar la imagen de Ubuntu Server (https://ubuntu.com/download/raspberry-pi)[https://ubuntu.com/download/raspberry-pi]

De forma alternativa se pueden encontrar versiones en el siguiente link:

(https://github.com/TheRemote/Ubuntu-Server-raspi4-unofficial/releases)[https://github.com/TheRemote/Ubuntu-Server-raspi4-unofficial/releases]

2. Flashar SD con imagen

Las credenciales por defecto son:
```
user: ubuntu
pass: ubuntu
```

3. Actualizar sistema
```
sudo apt-get update
sudo apt-get upgrade
```

4. Instalar ROS
```
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654

sudo apt-get upgrade
sudo apt-get install ros-melodic-desktop

sudo rosdep init
rosdep update

echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc
source ~/.bashrc
```

5. Instalar dependencias
```
sudo apt-get install cmake python-catkin-pkg python-empy python-nose python-setuptools libgtest-dev python-rosinstall python-rosinstall-generator python-wstool build-essential git
```


