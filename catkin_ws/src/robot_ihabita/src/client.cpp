#include "ros/ros.h"
#include "sensor_msgs/LaserScan.h"
#include <gsl/gsl_fit.h>

#define RAD2DEG(x) ((x)*180./M_PI)
#define DEG2RAD(x) ((x)*M_PI/180.)

//void getX(float distance)
//{
//    distance * cos()
//}


void scanCallback(const sensor_msgs::LaserScan::ConstPtr& scan)
{
    /*
    int count = scan->scan_time / scan->time_increment;
    ROS_INFO("I heard a laser scan %s[%d]:", scan->header.frame_id.c_str(), count);
    ROS_INFO("angle_range, %f, %f", RAD2DEG(scan->angle_min), RAD2DEG(scan->angle_max));
    */

    int front_left_limit = 330, front_right_limit = 30;
    /*
    std::vector<float> frontLeftDistances(60), frontRightDistances(60);

    frontLeftDistances = std::vector<float>(scan->ranges.begin() + 300, scan->ranges.end());
    frontRightDistances = std::vector<float>(scan->ranges.begin(), scan->ranges.begin() + frontRightLimit);

    std::vector<float> frontDistances(frontLeftDistances);
    frontDistances.insert(frontDistances.end(), frontRightDistances.begin(), frontRightDistances.end());

    std::vector<float> x(frontDistances), y(frontDistances);
    
    std::for_each(x.begin(), x.end(), &getX);
    */

    std::vector<float> x_axis, y_axis;
    for(int i = 0; i < scan->ranges.size(); i++) {
        float distance = scan->ranges[i];
        if ((front_left_limit <= i ||  i <= front_right_limit) && !isinf(distance)) {
            float x = distance * cos(DEG2RAD(i)), y = distance * sin(DEG2RAD(i));
            x_axis.push_back(y), y_axis.push_back(x);
            //std::cout << "x " << x << std::endl;
        }
    }

    for(int i = 0; i < scan->ranges.size(); i++) {
        float distance = scan->ranges[i];
        std::cout << i << ";" << distance << std::endl;
    }

    /*
    std::cout << "eje x original" << std::endl;
    for(int i = 0; i < x_axis.size(); i++)
        std::cout << i << ": " << x_axis[i] << std::endl;

    std::cout << "eje y original " << std::endl;
    for(int i = 0; i < y_axis.size(); i++)
        std::cout << i << ": " << y_axis[i] << std::endl;
    */
    /*
    int n = x_axis.size();
    double xs[n], ys[n];
    std::copy(x_axis.begin(), x_axis.end(), xs), std::copy(y_axis.begin(), y_axis.end(), ys);
    std::cout << "eje x" << std::endl;
    for(int i = 0; i < x_axis.size(); i++)
        std::cout << i << ": " << xs[i] << std::endl;

    std::cout << "eje y" << std::endl;
    for(int i = 0; i < y_axis.size(); i++)
        std::cout << i << ": " << ys[i] << std::endl;
    */
    /*
    double c0, c1, cov00, cov01, cov11, chisq;
    gsl_fit_linear(xs, 1, ys, 1, n, &c0, &c1, &cov00, &cov01, &cov11, &chisq);
    printf ("m: %g\n", round(c1));
    */

    //std::cout << "m: " << c1 << " " << round(c1) << std::endl;
    /*
    if (round(c1) == 0)
        std::cout << "muralla" << std::endl;
    else
        std::cout << "desconocido" << std::endl;
    */

    /*
    for(int i = 0; i < frontLeftDistances.size(); i++)
        std::cout << i << ": " << frontLeftDistances[i] << std::endl;

    for(int i = 0; i < frontRightDistances.size(); i++)
        std::cout << i << ": " << frontRightDistances[i] << std::endl;

    for(int i = 0; i < frontDistances.size(); i++)
        std::cout << i << ": " << frontDistances[i] << std::endl;
    */

    /*
    double x = (scan->ranges[90] + scan->ranges[270]) * 100.0;
    double y = (scan->ranges[0] + scan->ranges[180]) * 100.0;
    if (isinf(x))
        std::cout << "true" << std::endl;
    else
        std::cout << "false" << std::endl;
    std::cout << "x: " << x  << " isinf?:  " << isinf(x) << std::endl;
    */

    //scan->ranges.size()
    //std::cout << "ranges: " << scan->ranges.size() << std::endl;
    /*
    std::cout << "range 0: " << scan->ranges[0] << std::endl;
    std::cout << "range 90: " << scan->ranges[90] << std::endl;
    std::cout << "range 180: " << scan->ranges[180] << std::endl;
    std::cout << "range 270: " << scan->ranges[270] << std::endl;
    for(int i = 0; i < count; i++) {
        float degree = RAD2DEG(scan->angle_min + scan->angle_increment * i);
        int idegree = trunc(degree);
        std::cout << "range " << i  << ": " << scan->ranges[i] << std::endl;
        //if (idegree == 0 || idegree == 90 || idegree == -90 || idegree == 179)
        //if (idegree == 90)
        //    ROS_INFO(": [%f, %f]", degree, scan->ranges[i]);      
    }
    */
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "robot_node_client");
    ros::NodeHandle n;

    ros::Subscriber sub = n.subscribe<sensor_msgs::LaserScan>("/scan", 1000, scanCallback);

    ros::spin();

    return 0;
}
