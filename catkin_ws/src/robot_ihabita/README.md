
```bash
# in terminal 0
cd catkin_ws
catkin_make
source devel/setup.bash

# in new terminal 1:
roscore

# in terminal 0
roslaunch robot_ihabita test_rplidar.launch
```

Update project
```bash
cd catkin_ws
catkin_make
```